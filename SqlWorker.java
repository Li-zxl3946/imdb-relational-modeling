import java.sql.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

class FunctionalDependency
{
	public ArrayList<String> antecedent;
	public String consequent;
	
	public String listToString(ArrayList<String> slist)
	{
		String rtnStr = "[";
		for (String token : slist)
		{
			rtnStr += token + " ";
		}
		rtnStr += "]";
		return rtnStr;
	}
	
	public String toString()
	{
		return listToString(this.antecedent) + " -> " + this.consequent;
	}

}

public class SqlWorker {

	private String jdbc_driver = "";
	private String db_url = "jdbc:mysql://localhost:3306/hw3?autoReconnect=true&useSSL=true";
	private String userName = "user1";
	private String password = "password1";
	private Connection db_connection = null;
	private long sysTime;
	private ArrayList<String> attributes;
	private ArrayList<ArrayList<String>> movieData;
	private int NumOfRecord;
	private HashMap<String, ArrayList<ArrayList<Integer>>> moviePartitions;
	
	public SqlWorker()
	{
	}
	
	public void connectDatabase()
	{
		try
		{
			Class.forName("com.mysql.jdbc.Driver");

		    // Open a connection
		    // System.out.print("Connecting to database...");
		    db_connection = DriverManager.getConnection(db_url, userName, password);
		    if (!db_connection.isClosed() || db_connection != null)
		    {
		    	// System.out.println(" Connected");
		    }
		    
			// Do not auto commit
			db_connection.setAutoCommit(false);
			
		}
		catch (ClassNotFoundException e)
		{
			e.printStackTrace();
		}
		catch(SQLException e)
		{
			// System.out.println(" Fail");
			
			// Catch Exception Close DataBase	
			try
			{
				if (db_connection != null)
				{
					db_connection.close();
				}
			}
			catch (SQLException e1) 
			{
				// TODO Auto-generated catch block
				e1.printStackTrace();
			};
		}
	}	

	public void closeDatabase()
	{
	    try 
	    {
			if (!db_connection.isClosed() || db_connection != null)
			{
				db_connection.close();
			}
		} 
	    catch (SQLException e)
	    {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}	

	public void commitSQL()
	{
		try
		{
			// Commit Statement
			db_connection.commit();
		}
		catch(SQLException e)
		{
			try 
			{
				if (db_connection != null)
				{
					db_connection.rollback();
				}
			}
			catch (SQLException e1) 
			{
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
	}
	
	public void executeSQL(String SQLquery)
	{
		PreparedStatement sqlStmt = null;
		
		try
		{
			// Prepare statement 
			sqlStmt = db_connection.prepareStatement(SQLquery);
			sqlStmt.executeUpdate();
			this.commitSQL();
		}
		catch(SQLException e)
		{
			System.out.println(e.getMessage());
		}
		finally
		{
			try 
			{
				if (sqlStmt != null)
				{
					sqlStmt.close();
				}
			} 
			catch (SQLException e) 
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	public void startTaskTimer(String taskName)
	{
		System.out.print(taskName + "...... ");
		this.sysTime = System.currentTimeMillis();
	}
	
	public void endTaskTimer(String taskMsg)
	{
		long elaspeTime = System.currentTimeMillis() - this.sysTime;
		double doubleElaspeTime = (double)elaspeTime / 1000.0;
		System.out.println(taskMsg + " (" + doubleElaspeTime + " sec)");
	}
	
	public void startTask1()
	{
		this.startTaskTimer("Task 1 - Import movie data to new_movie table");

		this.executeSQL(
			"create table new_movie\r\n" + 
			"as\r\n" + 
			"(\r\n" + 
			"select \r\n" + 
			"	movie.id as movieId, \r\n" + 
			"	movie.type, \r\n" + 
			"	movie.startYear, \r\n" + 
			"	movie.runtime, \r\n" + 
			"	movie.numVotes, \r\n" + 
			"	mg.mg_genre_id as genreId, \r\n" + 
			"	mg.mg_genre as genre,\r\n" + 
			"	amr.amr_member_id as memberId,\r\n" + 
			"	amr.amr_birth_year as birthYear,\r\n" + 
			"	amr.amr_role as role\r\n" + 
			"from movie\r\n" + 
			"inner join \r\n" + 
			"(\r\n" + 
			"	select \r\n" + 
			"		actor_movie_role.movie as amr_movie, \r\n" + 
			"		member.id as amr_member_id,\r\n" + 
			"		member.birthYear as amr_birth_year,\r\n" + 
			"		role.role as amr_role\r\n" + 
			"	from actor_movie_role\r\n" + 
			"	inner join role on actor_movie_role.role = role.id\r\n" + 
			"	inner join member on actor_movie_role.actor = member.id\r\n" + 
			"	where actor_movie_role.movie not in\r\n" + 
			"	(\r\n" + 
			"		select actor_movie_role.movie as movie_id\r\n" + 
			"		from actor_movie_role\r\n" + 
			"		group by actor_movie_role.movie, actor_movie_role.actor\r\n" + 
			"		having count(actor_movie_role.role) > 1\r\n" + 
			"	)\r\n" + 
			")\r\n" + 
			"amr on movie.id = amr.amr_movie \r\n" + 
			"inner join \r\n" + 
			"(\r\n" + 
			"	select \r\n" + 
			"		genre.id as mg_genre_id, \r\n" + 
			"		genre.genre as mg_genre, \r\n" + 
			"		movie_genre.movie as mg_movie\r\n" + 
			"	from movie_genre\r\n" + 
			"	inner join genre on movie_genre.genre = genre.id\r\n" + 
			")\r\n" + 
			"mg on movie.id = mg.mg_movie\r\n" + 
			"where \r\n" + 
			"movie.runtime >= 90\r\n" + 
			");"
		);
		
		this.executeSQL("ALTER TABLE new_movie ADD id int;");
		this.executeSQL("SET @i = 0");
		this.executeSQL("UPDATE new_movie SET id = @i:=@i+1;");
		this.executeSQL("ALTER TABLE new_movie ADD PRIMARY KEY (id);");
		
		this.endTaskTimer("Done");
	}
	
	public void startTask2()
	{
		this.startTaskTimer("Task 2 - Naive Functional Dependency Discovery");
		
		this.naiveFindFD();
		
		this.endTaskTimer("Task Completed");
	}

	public void startTask3()
	{
		this.startTaskTimer("Task 3");
		
		System.out.println();
		
		this.loadPartition();		
			
		this.improvedFindFd();
		
		this.endTaskTimer("Done");
	}

	public void loadPartition()
	{	
		try
		{
			moviePartitions = new HashMap<String, ArrayList<ArrayList<Integer>>>();
			moviePartitions.put("@EMPTY", new ArrayList<ArrayList<Integer>>());
			
			for (String att : this.attributes)
			{
				ArrayList<ArrayList<Integer>> partition = new ArrayList<ArrayList<Integer>>();
				
				PreparedStatement st = db_connection.prepareStatement(
						"SELECT id," + att + " FROM new_movie ORDER BY " + att + ", id"
				);
				
				ResultSet rs = st.executeQuery();
				rs.next();
				
				String curData = rs.getString(2);
				String preData = curData;
				
				int curRowId = rs.getInt(1);
				
				ArrayList<Integer> equiClass = new ArrayList<Integer>();
				equiClass.add(curRowId);
				
				while (rs.next())
				{
					curData = rs.getString(2);
					curRowId = rs.getInt(1);
					
					if ( preData == curData ||
						 (preData != null && preData.equals(curData)) )
					{
						equiClass.add(curRowId);
					}
					else
					{
						if (!equiClass.isEmpty())
						{
							Collections.sort(equiClass);
							partition.add(equiClass);
						}
						
						preData= curData;
						equiClass = new ArrayList<Integer>();
						equiClass.add(curRowId);
					}
				}
				
				if (!equiClass.isEmpty())
				{
					Collections.sort(equiClass);
					partition.add(equiClass);
				}
				
				moviePartitions.put(att, partition);
				System.out.println(att + " => " + partition.size());
				st.close();
				
			}
			
			System.out.println("Load Partition Done");
		}
		catch (SQLException e)
		{
			System.out.println(e.getMessage());
		}
	}
	
	public ArrayList<ArrayList<String>> getAlphaCombination(int level, ArrayList<String> aSet)
	{
		ArrayList<ArrayList<String>> rtnList = new ArrayList<ArrayList<String>>();
		
		if (level == 0)
		{
			ArrayList<String> alphaSet = new ArrayList<String>();
			alphaSet.add("@EMPTY");
			rtnList.add(alphaSet);
		}
		else
		{
			ArrayList<String> tmpAset = new ArrayList<String>();
			tmpAset.addAll(aSet);
			
			for (String alpha : aSet)
			{
				tmpAset.remove(alpha);
				
				ArrayList<ArrayList<String>> alphaList = this.getAlphaCombination((level-1), tmpAset);
				
				for (int i = 0; i < alphaList.size(); i++)
				{
					ArrayList<String> combineAlpha = new ArrayList<String>();
					combineAlpha.add(alpha);
					if(level > 1)
					{
						combineAlpha.addAll(alphaList.get(i));
					}
					
					rtnList.add(combineAlpha);
				}
			}
		}
		
		return rtnList;
	}
	
	public String listToString(ArrayList<String> hs)
	{
		String rtnStr = "[";
		for (String token : hs)
		{
			rtnStr += token + " ";
		}
		rtnStr += "]";
		return rtnStr;
	}
	
	public void improvedFindFd()
	{	
		ArrayList<ArrayList<String>> alphaList = this.getAlphaCombination(1, this.attributes);
		alphaList.addAll(this.getAlphaCombination(2, this.attributes));

		int count = 0;
		
		HashSet<FunctionalDependency> fdSet = new HashSet<FunctionalDependency>();
		
		for (ArrayList<String> alpha : alphaList)
		{
			count++;

			System.out.println("\n#" + count + " Alpha:" + this.listToString(alpha) + " current Fd size:" + fdSet.size());
			
			for (FunctionalDependency fd : fdSet)
			{
				System.out.println(fd.toString());
			}
			
			boolean pruneAlpha = false;
			
			for (Iterator<FunctionalDependency> fdIt = fdSet.iterator(); 
					!pruneAlpha && fdIt.hasNext();
			)
			{
				FunctionalDependency fd = fdIt.next();
				pruneAlpha = alpha.contains(fd.consequent) && alpha.containsAll(fd.antecedent);
			}
			
			if (pruneAlpha) 
			{
				continue;
			}
			
			ArrayList<String> RHS = new ArrayList<String>();
			
			// if attributes is the minimal
			for(String a : this.attributes)
			{
				if (!alpha.contains(a))
				{
					boolean isMinimal = true;
					
					for ( Iterator<String> attIt = this.attributes.iterator();
						isMinimal && attIt.hasNext();
					)
					{
						String b = attIt.next();
						
						for (Iterator<FunctionalDependency> fdIt = fdSet.iterator();
								isMinimal && fdIt.hasNext();)
						{
							FunctionalDependency fd = fdIt.next();
							ArrayList<String> alphaPrime = new ArrayList<String>(alpha);
							alphaPrime.remove(a);
							alphaPrime.remove(b);
														
							isMinimal = !(
									fd.consequent.equals(b) && 
									fd.antecedent.containsAll(alphaPrime) &
									alphaPrime.containsAll(fd.antecedent)
							);
						}
					}
					
					if (isMinimal)
					{
						//System.out.println(a + "is added to RHS");
						RHS.add(a);
					}
				}
			}
			
			for(String a : RHS)
			{
				boolean foundInFd = false;
				// TODO Discard if alpha --> a is already in FD

				for (FunctionalDependency fd : fdSet)
				{
					if (fd.antecedent.containsAll(alpha) && fd.consequent.equals(a))
					{
						foundInFd = true;
						break;
					}
				}
				
				if (foundInFd)
				{
					continue;
				}

				ArrayList<ArrayList<Integer>> alphaPartition = new ArrayList<ArrayList<Integer>>();
				
				for(String att : alpha)
				{
					// Intersection
					if (alphaPartition.isEmpty() )
					{
						alphaPartition = new ArrayList<ArrayList<Integer>>(moviePartitions.get(att));
					}
					else
					{
						alphaPartition = this.intersect(alphaPartition, moviePartitions.get(att));
					}
				}

				// Refinement Check
				if(this.refinementCheck(alphaPartition, this.moviePartitions.get(a)))
				{
					//System.out.print("refinement check: ");
					
					FunctionalDependency fd = new FunctionalDependency();
					fd.antecedent = alpha;
					fd.consequent = a;
					
					fdSet.add(fd);
					//System.out.println("Add " + this.hsToString(fd.antecedent) + " -> " + a);
				}

			}

		}
		
		for (FunctionalDependency fd : fdSet)
		{
			System.out.println(fd.toString());
		}
	}
		
	public ArrayList<ArrayList<Integer>> intersect(ArrayList<ArrayList<Integer>> p1,  ArrayList<ArrayList<Integer>> p2)
	{
		int count = 0;
		ArrayList<ArrayList<Integer>> retList= new  ArrayList<ArrayList<Integer>>();
		for (int i = 0; i < p1.size(); i++)
		{
			count++;
			ArrayList<Integer> list1 = new ArrayList<Integer>(p1.get(i));
			for (int j = 0; j < p2.size(); j++)
			{
				ArrayList<Integer> list2 = new ArrayList<Integer>(p2.get(j));
				ArrayList<Integer> mergeList = new ArrayList<Integer>();

				int index_1 = 0;
				int size_1 = list1.size();
				
				int index_2 = 0;
				int size_2 = list2.size();
								
				while(true)
				{
					int val1 = list1.get(index_1).intValue();
					int val2 = list2.get(index_2).intValue();
					
					if ( val1 > val2  )
					{
						index_2++;
					}
					else if ( val1 < val2 )
					{
						index_1++;
					}
					else
					{
						mergeList.add(val1);
						index_1++;
						index_2++;
					}
								
					if (index_1 >= size_1)
					{
						break;
					}
					if (index_2 >= size_2)
					{
						break;
					}
				}
				
				if (!mergeList.isEmpty())
				{
					retList.add(mergeList);
				}
				
			}

		}

		return retList;
	}
		
	public boolean refinementCheck(ArrayList<ArrayList<Integer>> pi, ArrayList<ArrayList<Integer>> piPrime)
	{
		boolean retBool = true;
		
		ArrayList<HashSet<Integer>> piSet = new ArrayList<HashSet<Integer>>();
		for (ArrayList<Integer> piIt : pi)
		{
			piSet.add(new HashSet<Integer>(piIt));
		}
		
		ArrayList<HashSet<Integer>> piPrimeSet = new ArrayList<HashSet<Integer>>();
		for (ArrayList<Integer> piPrimeIt : piPrime)
		{
			piPrimeSet.add(new HashSet<Integer>(piPrimeIt));
		}		
		
		for (HashSet<Integer> piIt : piSet)
		{
			if (retBool == false)
			{
				break;
			}
			
			boolean subsetFound= false;
			
			for (HashSet<Integer> piPrimeIt : piPrimeSet)
			{
				if (subsetFound == true)
				{
					break;
				}
				
				subsetFound = piPrimeIt.containsAll(piIt);				
				retBool = subsetFound;
			}
		}
		
		return retBool;
	}
		
	public void loadMovieData()
	{
		this.startTaskTimer("Load Data from New Movie Table");
		
		Statement sqlStmt = null;
		String sqlQuery = 
				"select \r\n" + 
				"movieId, \r\n" + 
				"type, \r\n" + 
				"startYear, \r\n" + 
				"runtime, \r\n" + 
				"numVotes, \r\n" + 
				"genreId, \r\n" + 
				"genre, \r\n" + 
				"memberId, \r\n" + 
				"birthYear, \r\n" + 
				"role \r\n" + 
				"from new_movie \r\n" + 
				"ORDER BY id;";
		
		attributes = new ArrayList<String>();
		movieData = new ArrayList<ArrayList<String>>();
		
		try
		{
			int counter = 0;
			sqlStmt = db_connection.createStatement();
			ResultSet rs = sqlStmt.executeQuery(sqlQuery);
		    ResultSetMetaData rsmd = rs.getMetaData();
		    int columnsNumber = rsmd.getColumnCount();
		    
		    // Update Attributes
	        for (int i = 1; i <= columnsNumber; i++)
	        {
	        	attributes.add(rsmd.getColumnName(i));
	        	movieData.add(new ArrayList<String>());
	        }
		    
			while(rs.next())
			{
		        for (int i = 1; i <= columnsNumber; i++)
		        {
		            movieData.get((i-1)).add(rs.getString(i));
		        }
			}
			
			NumOfRecord = movieData.get(0).size();
		}
		catch(SQLException e)
		{
			System.out.println(e.getMessage());
		}
		
		this.endTaskTimer(
				"Done, " +
				"Num of Attribute = " + movieData.size() + 
				", Num of Records = " + movieData.get(0).size() );
	}
	
	
	public HashSet<Integer> getAttributeSet(int num, int numOfAttribute)
	{
		HashSet<Integer> retSet = new HashSet<Integer>();

		for (int pos = 0; pos < numOfAttribute; pos++)
		{
			if (((num >> pos) & 1) != 0)
			{
				retSet.add(pos);
			}
		}
		
		return retSet;
	}
	
	
	public HashSet<Integer> getConsequenceSet(HashSet<Integer> aSet, int numOfAttribute)
	{
		HashSet<Integer> retSet = new HashSet<Integer>();
		
		for (int pos = 0; pos < numOfAttribute; pos++)
		{
			if (!aSet.contains(pos))
			{
				retSet.add(pos);
			}
		}
		
		return retSet;
	}
	
	
	public String getAttDataAt(HashSet<Integer> attDataSet, int row)
	{
		String rtnStr = "";
		
		for(int attNum : attDataSet)
		{
			rtnStr += this.movieData.get(attNum).get(row) + "&&";
		}
		
		return rtnStr;
	}
	
	
	public String getAttDataAt(int att, int row)
	{
		return this.movieData.get(att).get(row);
	}
	
	
	public boolean isFD(HashSet<Integer> attSet, int conseq)
	{
		HashSet<String> fdSet = new HashSet<String>();
		
		HashMap<String, String> fdSetMap= new HashMap<String, String>();
		
		for (int row = 0; row < this.NumOfRecord; row++)
		{
			String attStr = this.getAttDataAt(attSet, row);
			String conseqStr = this.getAttDataAt(conseq, row);
			if(fdSetMap.isEmpty())
			{
				fdSetMap.put(attStr, conseqStr);
			}
			else
			{
				if (fdSetMap.containsKey(attStr))
				{
					if ( 	
							!(
							(fdSetMap.get(attStr) == conseqStr) || 
							(fdSetMap.get(attStr) != null && fdSetMap.get(attStr).equals(conseqStr))
							)
					)
					{
						return false;
					}
				}
				else
				{
					fdSetMap.put(attStr, conseqStr);
				}
			}
		}
		
		return true;
	}
	
	
	public String fdToString(HashSet<Integer> attSet, int conseq)
	{
		String rtnStr = "( ";
		
		for (int att : attSet)
		{
			rtnStr += this.attributes.get(att) + " ";
		}
		
		rtnStr += ") => " + this.attributes.get(conseq);
		
		return rtnStr;
	}
	
	
	public void naiveFindFD()
	{
		int numOfAttribute = this.attributes.size();
		int maxCombination = (int)Math.pow(2, numOfAttribute);
				
		for (int combination = 1; combination < maxCombination; combination++)
		{
			HashSet<Integer> attSet = this.getAttributeSet(combination, numOfAttribute);
			HashSet<Integer> conseqSet = this.getConsequenceSet(attSet, numOfAttribute);
					
			for (int conseq : conseqSet)
			{		
				System.out.println(this.fdToString(attSet, conseq));
				if ( this.isFD(attSet, conseq) )
				{
					System.out.println(this.fdToString(attSet, conseq));
				}
			}
			
		}
	}
		
	public void startAllTasks()
	{
		this.connectDatabase();
		
		//this.startTask1();
		
		this.loadMovieData();
		
		//this.startTask2();
		
		this.startTask3();
		
		this.closeDatabase();
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		SqlWorker worker = new SqlWorker();
		
		worker.startAllTasks();
	}

}
